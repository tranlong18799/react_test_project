import './App.css';
import {Routes,Route} from 'react-router-dom';

import LayoutComponent from './Layout/LayoutComponent';
import CallingComponent from './Components/PhoneUI/CallingComponent';
import PhoneUIComponent from './Components/PhoneUI/PhoneUIComponent';
function App() {
  return (
    <Routes>
      <Route path={'/'} element={<LayoutComponent component={<PhoneUIComponent />} />} />
      <Route path={'/call/:id'} element={<LayoutComponent component={<CallingComponent />} />} />
    </Routes>
    // <>
    //   <Dial />
    // </>
  );
}

export default App;
