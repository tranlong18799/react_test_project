import React, { useState } from "react";
import { CloseOutlined, PhoneOutlined } from "@ant-design/icons";
import PhoneUIComponent from "../Components/PhoneUI/PhoneUIComponent";

const width= 250;
export default function LayoutComponent({component}) {
  const [phone, setPhone] = useState("");
  const handleChange = (e) => {
    console.log(e.target.value);
    setPhone(e.target.value)
  };
  const handleClick = (values)=>{
    console.log(values);
    setPhone(`${phone+values}`);
  }

  const handleClose = ()=>{
    setPhone('');
  }
  return (
    <div className="container">
      <div className="py-4" style={{width:230}}>
        <div className="bg-purple-700 flex flex-col gap-y-2 p-4 rounded-md">
          {component}
        </div>
      </div>
    </div>
  );
}
