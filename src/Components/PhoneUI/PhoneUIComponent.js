import { PhoneOutlined } from "@ant-design/icons";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { SipProvider } from "react-sip";
import Dialpad from "./Dialpad";

export default function PhoneUIComponent() {
  const navigate = useNavigate();
  const [phone, setPhone] = useState("");
  const handleChange = (e) => {
    console.log(e.target.value);
    setPhone(e.target.value);
  };
  const handleClick = (values) => {
    console.log(values);
    setPhone(`${phone + values}`);
  };

  const handleClose = () => {
    setPhone("");
  };
  return (
    <>
      <div className="flex flex-row text-white">
      <SipProvider
        host="proxy01.buniapp.com"
        pathname="wss://sbc03.tel4vn.com:7444"
        username="105"
        password="test1105"
        sessionTimersExpires={120}
        iceServers={[
          {
            urls: "stun:105@2-test1.gcalls.vn:50061"
          },
          {
            urls: "turn:105@2-test1.gcalls.vn:50061",
            username: "long",
            credential: "daiduong1871999@gmail.com"
          }
        ]}
        debug={true}
        autoRegister={true} // true by default, see jssip.UA option register
        autoAnswer={false}
      >
        <Dialpad phone={phone} handleChange={handleChange} handleClose={handleClose} />
      </SipProvider>
      </div>
      <div className="flex flex-row text-white">
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("1")}
        >
          1
        </div>
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("2")}
        >
          <div className="flex flex-col items-center">
            <label>2</label>
            <label className="text-sl">ABC</label>
          </div>
        </div>
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("3")}
        >
          <div className="flex flex-col items-center">
            <label>3</label>
            <label className="text-sl">DEF</label>
          </div>
        </div>
      </div>
      <div className="flex flex-row text-white">
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("4")}
        >
          <div className="flex flex-col items-center">
            <label>4</label>
            <label className="text-sl">GHI</label>
          </div>
        </div>
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("5")}
        >
          <div className="flex flex-col items-center">
            <label>5</label>
            <label className="text-sl">JKL</label>
          </div>
        </div>
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("6")}
        >
          <div className="flex flex-col items-center">
            <label>6</label>
            <label className="text-sl">MNO</label>
          </div>
        </div>
      </div>
      <div className="flex flex-row text-white">
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("7")}
        >
          <div className="flex flex-col items-center">
            <label>7</label>
            <label className="text-sl">PQRS</label>
          </div>
        </div>
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("8")}
        >
          <div className="flex flex-col items-center">
            <label>8</label>
            <label className="text-sl">TUV</label>
          </div>
        </div>
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("9")}
        >
          <div className="flex flex-col items-center">
            <label>9</label>
            <label className="text-sl">WXYZ</label>
          </div>
        </div>
      </div>
      <div className="flex flex-row text-white">
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("*")}
        >
          *
        </div>
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("0")}
        >
          <div className="flex flex-col items-center">
            <label>0</label>
            <label className="text-sl">*</label>
          </div>
        </div>
        <div
          className="w-1/3 flex justify-center cursor-pointer"
          onClick={() => handleClick("#")}
        >
          #
        </div>
      </div>
      <div className="flex flex-row text-white">
        <div className="w-full flex justify-center">
          <button
            className="w-2/3 rounded-lg 
              bg-emerald-500 shadow-xl p-2 
              focus:ring-2 focus:ring-emerald-600"
            onClick={() => navigate(`/call/${phone}`)}
          >
            <PhoneOutlined size={100} />
          </button>
        </div>
      </div>
    </>
  );
}
