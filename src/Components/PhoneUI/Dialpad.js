import React from "react";
import { CloseOutlined } from "@ant-design/icons";

class Dialpad extends React.Component {
  constructor(props) {
    super();
    this.props = props;
  }

  onCall() {
    const { destination } = this.state;
    // const {
    //   startCall,
    //   sip: { status: sipStatus },
    //   call: { status: callStatus }
    // } = this.props;

    // if (!destination) {
    //   return;
    // }
    // if (sipStatus !== SIP_STATUS_REGISTERED) {
    //   return;
    // }
    // if (callStatus !== CALL_STATUS_IDLE) {
    //   return;
    // }

    // ReactSip.SipProvider.childContextTypes.startCall(destination);
  }

  render() {
    const { phone, handleChange,handleClose } = this.props;
    return (
      <React.Fragment>
        <div className="relative w-full">
          <input
            type="search"
            id="search-dropdown"
            className="block p-2 w-full z-20 text-sm 
                text-white bg-transparent 
                border-b-2 border-gray-300
                focus:ring-blue-500 focus:border-blue-500"
            required
            value={phone}
            onChange={handleChange}
          />
          <button
            type="submit"
            className="absolute top-0 right-0 p-2 text-sm font-medium text-white"
            onClick={handleClose}
          >
            <CloseOutlined size={40} />
          </button>
        </div>
      </React.Fragment>
    );
  }
}

export default Dialpad;
