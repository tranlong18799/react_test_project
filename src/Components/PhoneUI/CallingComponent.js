import {
  AudioOutlined,
  PauseOutlined,
  PhoneOutlined,
  StopOutlined,
  TableOutlined,
  UserOutlined,
} from "@ant-design/icons";
import React from "react";
import { useParams } from "react-router-dom";

export default function CallingComponent() {
  const {id} = useParams();
  return (
    <div className="w-full flex flex-col gap-y-2 flex-wrap text-white">
      <div className="flex flex-row gap-4">
        <div className="w-1/4 flex justify-center items-center">
          <div className="flex justify-center border-2 border-white rounded-full p-1">
          <UserOutlined />
          </div>
        </div>
        <div className="w-3/4 flex flex-col justify-center items-start text-lg">
          Trần Long
        </div>
      </div>
      <div className="flex justify-center items-center text-sm">{id}</div>
      <div className="flex justify-center items-center text-xs">Gcalls</div>
      <div className="flex justify-center items-center pt-4 text-sm">Đang gọi...</div>
      <div className="flex gap-2 justify-center items-center pt-10">
        <div className="w-1/4 opacity-50">
          <div className="border-2 border-slate-400 rounded-full p-3 flex ">
            <AudioOutlined />
          </div>
          <div className="text-xs flex justify-center">mute</div>
        </div>
        <div className="w-1/4 opacity-50">
          <div className="border-2 border-slate-400 rounded-full p-3 flex ">
            <TableOutlined />
          </div>
          <div className="text-xs flex justify-center">keypad</div>
        </div>
        <div className="w-1/4 opacity-50">
          <div className="border-2 border-slate-400 rounded-full p-3 flex ">
            <PauseOutlined />
          </div>
          <div className="text-xs flex justify-center">pause</div>
        </div>
        <div className="w-1/4 opacity-50">
          <div className="border-2 border-slate-400 rounded-full p-3 flex ">
            <PhoneOutlined />
          </div>
          <div className="text-xs flex justify-center">forward</div>
        </div>
      </div>
      <div className="flex items-end justify-center pt-20">
        <div className="w-full flex justify-center">
          <button className="p-3 w-1/4 rounded-lg flex justify-center shadow-2xl bg-red-600">
            <StopOutlined />
          </button>
        </div>
      </div>
    </div>
  );
}
